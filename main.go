package GoTelegraph

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"
	"strings"
)

var quoteEscaper = strings.NewReplacer("\\", "\\\\", `"`, "\\\"")

type respJSON []struct {
	Src string `json:"src"`
}

func Upload(file string) (string, error) {
	client := &http.Client{}
	values := map[string]io.Reader{
		"file": mustOpen(file), // lets assume its this file
	}
	urls, err := send(client, "https://telegra.ph/upload", values)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("https://telegra.ph%s", urls[0].Src), err
}

func escapeQuotes(s string) string {
	return quoteEscaper.Replace(s)
}

func send(client *http.Client, url string, values map[string]io.Reader) (jsonResponse respJSON, err error) {
	// Prepare a form that you will submit to that URL.
	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	for key, r := range values {
		if x, ok := r.(io.Closer); ok {
			defer x.Close()
		}
		var fw io.Writer
		// Add an image file
		if f, ok := r.(*os.File); ok {

			fmt.Printf("Open .. %s\n", f.Name())
			h := make(textproto.MIMEHeader)
			h.Set("Content-Disposition",
				fmt.Sprintf(`form-data; name="%s"; filename="%s"`,
					escapeQuotes(key), escapeQuotes(f.Name())))
			h.Set("Content-Type", getMimeType(f.Name()))
			fw, err = w.CreatePart(h)
			if err != nil {
				return nil, err
			}

		} else {
			// Add other fields
			if fw, err = w.CreateFormField(key); err != nil {
				return nil, err
			}
		}
		if _, err = io.Copy(fw, r); err != nil {
			return nil, err
		}

	}
	// Don't forget to close the multipart writer.
	// If you don't close it, your request will be missing the terminating boundary.
	w.Close()

	// Now that you have a form, you can submit it to your handler.
	req, err := http.NewRequest("POST", url, &b)
	if err != nil {
		return nil, err
	}
	// Don't forget to set the content type, this will contain the boundary.
	req.Header.Set("Content-Type", w.FormDataContentType())

	// Submit the request
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	// Check the response
	if res.StatusCode != http.StatusOK {
		err = fmt.Errorf("bad status: %s", res.Status)
	}
	bodyText, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}
	fmt.Printf("%s\n", bodyText)
	var jsonResp respJSON
	err = json.Unmarshal(bodyText, &jsonResp)

	if err != nil {
		log.Println(err.Error())
	}
	return jsonResp, err
}

func mustOpen(f string) *os.File {
	r, err := os.Open(f)
	if err != nil {
		panic(err)
	}
	return r
}

func getMimeType(filePath string) string {
	file, err := os.Open(filePath)
	if err != nil {
		log.Println(err.Error())
	}
	defer file.Close()
	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)
	_, err = file.Read(buffer)
	if err != nil {
		log.Println(err.Error())
	}

	// Reset the read pointer if necessary.
	file.Seek(0, 0)

	// Always returns a valid content-type and "application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)
	return contentType
}
